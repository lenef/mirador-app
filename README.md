# Mirador Cloudron app

This repository contains the Cloudron app package source for [Mirador](https://projectmirador.org/).

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/documentation/custom-apps/tutorial/#cloudron-cli).

```
cd mirador-app

cloudron build
cloudron install
```
