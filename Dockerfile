FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /app/code
WORKDIR /app/code

# Mirado version
ARG VERSION=2.7.0

# install Mirador
RUN wget https://github.com/ProjectMirador/mirador/releases/download/v${VERSION}/build.zip -O /tmp/build.zip  
RUN unzip /tmp/build.zip -d /tmp
RUN rm /tmp/build/mirador/mirador.js  
RUN mv /tmp/build/mirador /app/code/
RUN rm -rf /tmp/build* 

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
COPY apache/app.conf /etc/apache2/sites-enabled/app.conf
RUN echo "Listen 3000" > /etc/apache2/ports.conf

RUN chown -R www-data.www-data /app/code

COPY index.html start.sh /app/code/
COPY data.json /app/data/
RUN ln -s /app/data/data.json /app/code/data.json

CMD [ "/app/code/start.sh" ]
