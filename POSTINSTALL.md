This application comes with many examples. You can add your own IIIF manifests by selecting + icon.

You can change the default settings and the set of manifests by editing `data.json` file.
